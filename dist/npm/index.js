'use strict';
var core = require('./core');
var RippleAPI = require('./api');

module.exports = {
  RippleAPI: RippleAPI,
  _DEPRECATED: core // WARNING: this will be removed soon
};