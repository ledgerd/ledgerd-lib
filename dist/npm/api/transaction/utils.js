
'use strict';
var _ = require('lodash');
var async = require('async');
var BigNumber = require('bignumber.js');
var common = require('../common');
var composeAsync = common.composeAsync;

function setTransactionBitFlags(transaction, values, flags) {
  for (var flagName in flags) {
    var flagValue = values[flagName];
    var flagConversions = flags[flagName];

    if (flagValue === true && flagConversions.set !== undefined) {
      transaction.setFlags(flagConversions.set);
    }
    if (flagValue === false && flagConversions.unset !== undefined) {
      transaction.setFlags(flagConversions.unset);
    }
  }
}

function getFeeDrops(remote, callback) {
  var feeUnits = 10; // all transactions currently have a fee of 10 fee units
  remote.feeTxAsync(feeUnits, function (err, data) {
    callback(err, data ? data.to_text() : undefined);
  });
}

function formatPrepareResponse(txJSON) {
  var instructions = {
    fee: txJSON.Fee,
    sequence: txJSON.Sequence,
    maxLedgerVersion: txJSON.LastLedgerSequence
  };
  return {
    txJSON: JSON.stringify(txJSON),
    instructions: _.omit(instructions, _.isUndefined)
  };
}

function prepareTransaction(transaction, remote, instructions, callback) {
  common.validate.instructions(instructions);

  transaction.complete();
  var account = transaction.getAccount();
  var txJSON = transaction.tx_json;

  function prepareMaxLedgerVersion(callback_) {
    if (instructions.maxLedgerVersion !== undefined) {
      txJSON.LastLedgerSequence = parseInt(instructions.maxLedgerVersion, 10);
      callback_();
    } else {
      (function () {
        var offset = instructions.maxLedgerVersionOffset !== undefined ? parseInt(instructions.maxLedgerVersionOffset, 10) : 3;
        remote.getLedgerSequence(function (error, ledgerVersion) {
          txJSON.LastLedgerSequence = ledgerVersion + offset;
          callback_(error);
        });
      })();
    }
  }

  function prepareFee(callback_) {
    if (instructions.fee !== undefined) {
      txJSON.Fee = common.xrpToDrops(instructions.fee);
      callback_();
    } else {
      getFeeDrops(remote, composeAsync(function (serverFeeDrops) {
        if (instructions.maxFee !== undefined) {
          var maxFeeDrops = common.xrpToDrops(instructions.maxFee);
          txJSON.Fee = BigNumber.min(serverFeeDrops, maxFeeDrops).toString();
        } else {
          txJSON.Fee = serverFeeDrops;
        }
      }, callback_));
    }
  }

  function prepareSequence(callback_) {
    if (instructions.sequence !== undefined) {
      txJSON.Sequence = parseInt(instructions.sequence, 10);
      callback_(null, formatPrepareResponse(txJSON));
    } else {
      remote.findAccount(account).getNextSequence(function (error, sequence) {
        txJSON.Sequence = sequence;
        callback_(error, formatPrepareResponse(txJSON));
      });
    }
  }

  async.series([prepareMaxLedgerVersion, prepareFee, prepareSequence], common.convertErrors(function (error, results) {
    callback(error, results && results[2]);
  }));
}

module.exports = {
  setTransactionBitFlags: setTransactionBitFlags,
  prepareTransaction: prepareTransaction,
  common: common,
  promisify: common.promisify
};