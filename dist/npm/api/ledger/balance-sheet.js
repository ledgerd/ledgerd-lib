'use strict';

var _Object$assign = require('babel-runtime/core-js/object/assign')['default'];

var _ = require('lodash');
var utils = require('./utils');
var validate = utils.common.validate;
var composeAsync = utils.common.composeAsync;
var convertErrors = utils.common.convertErrors;

function formatBalanceSheet(balanceSheet) {
  var result = {};

  if (!_.isUndefined(balanceSheet.balances)) {
    result.balances = [];
    _.forEach(balanceSheet.balances, function (balances, counterparty) {
      _.forEach(balances, function (balance) {
        result.balances.push(_Object$assign({ counterparty: counterparty }, balance));
      });
    });
  }
  if (!_.isUndefined(balanceSheet.assets)) {
    result.assets = [];
    _.forEach(balanceSheet.assets, function (assets, counterparty) {
      _.forEach(assets, function (balance) {
        result.assets.push(_Object$assign({ counterparty: counterparty }, balance));
      });
    });
  }
  if (!_.isUndefined(balanceSheet.obligations)) {
    result.obligations = _.map(balanceSheet.obligations, function (value, currency) {
      return { currency: currency, value: value };
    });
  }

  return result;
}

function getBalanceSheetAsync(address, options, callback) {
  var _this = this;

  validate.address(address);
  validate.getBalanceSheetOptions(options);

  var requestOptions = _Object$assign({}, {
    account: address,
    strict: true,
    hotwallet: options.excludeAddresses,
    ledger: options.ledgerVersion
  });

  var requestCallback = composeAsync(formatBalanceSheet, convertErrors(callback));

  this.remote.getLedgerSequence(function (err, ledgerVersion) {
    if (err) {
      callback(err);
      return;
    }

    if (_.isUndefined(requestOptions.ledger)) {
      requestOptions.ledger = ledgerVersion;
    }

    _this.remote.requestGatewayBalances(requestOptions, requestCallback);
  });
}

function getBalanceSheet(address) {
  var options = arguments.length <= 1 || arguments[1] === undefined ? {} : arguments[1];

  return utils.promisify(getBalanceSheetAsync).call(this, address, options);
}

module.exports = getBalanceSheet;